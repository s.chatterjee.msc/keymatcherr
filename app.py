
'''

Flask application 


'''
import shutil
from flask import Flask,render_template,redirect
import os
import json
from flask import request, jsonify
from madMax import encode_quantum,dquantum_map
from datetime import datetime
import random
import cv2
UPLOAD_FOLDER = './uploads'
M_FOLDER='./all_images'
NGRAM = './new_image/'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}
app = Flask(__name__,template_folder='./template')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MODEL'] = M_FOLDER
app.config['NGRAM'] = NGRAM
@app.route('/')
def index():
   return render_template('index.html')




@app.route('/uploadfotMatch')
def upload_match_view():
   return render_template('upload2.html')

@app.route('/upload')
def upload_file_view():
   return render_template('upload.html')


def assure_dir(cdir):
    if not os.path.exists(cdir):
    	os.mkdir(cdir)
    	print("Making DIR")
    else:
    	try:
        	shutil.rmtree(cdir)
        	os.mkdir(cdir)
    	except FileNotFoundError as e:
        	print(e)



def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
   if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename =file.filename
            source=str(os.path.join(app.config['MODEL'], filename))
            file.save(os.path.join(app.config['MODEL'], filename))
            img=cv2.imread(source)
            img=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
            img=cv2.normalize(img, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
            cv2.imwrite(source,img )
            encode_quantum()
            return render_template('upload.html',name = filename)
   return render_template('upload.html')



@app.route('/matchfi', methods = ['GET', 'POST'])
def upload_match_file():
   if request.method == 'POST':
        # check if the post request has the file part
        assure_dir(app.config['NGRAM'])
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
        	now=random.randint(0,len(file.filename))
        	filename =file.filename
        	source=str(os.path.join(app.config['NGRAM'],filename))
        	file.save(os.path.join(app.config['NGRAM'],filename))
        	img=cv2.imread(source)
        	img=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        	img=cv2.normalize(img, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
        	cv2.imwrite(source,img )
        	encode_quantum()
        	fn=dquantum_map(filename)
        	return render_template('upload2.html',name = fn)
           
   return render_template('upload.html')
   


           
            


local_run = os.getenv('local_run', '').lower() == 'true'
if __name__ == '__main__':
    port=5000
    app.run(host='0.0.0.0', port=port, threaded=not local_run, debug=local_run)