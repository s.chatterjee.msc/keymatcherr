
FROM python:3.7
WORKDIR /home/application

EXPOSE 5000

RUN apt-get -y update
COPY  ./requirements.txt .
RUN pip3 install  -r requirements.txt

COPY  ./ .


CMD ["stdbuf", "-oL", "python3", "/home/application/app.py"]
