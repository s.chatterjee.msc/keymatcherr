
import os
import pickle
import shutil
import gridfs
from imagededup.methods import PHash,DHash
from pymongo import MongoClient
from pymongo.errors import PyMongoError
import pandas as pd
from PIL import Image
import cv2 
import numpy as np
global imgdir, dictdir
imgdir = './all_images/'
pimgdir='./process'
nimgdir = './new_image/'
dictdir = './mad/'
mstrdir = '/home/application/RIS/mstr_dict/'
SHARED_LEARNING = os.getenv('SHARED_LEARNING', False)
Image.MAX_IMAGE_PIXELS = None
phasher = DHash()

def assure_dir(cdir):
    if not os.path.exists(cdir):
        os.mkdir(cdir)


def encode_quantum():
    assure_dir(dictdir)
    encoded_images = phasher.encode_images(imgdir)
    assure_dir(dictdir)
    try:
        os.remove(dictdir + 'mad.pickle')
    except FileNotFoundError as e:
        pass
    with open(dictdir + 'mad.pickle', 'wb') as file:
        z = file.write(pickle.dumps(encoded_images))

def get_dict():
    pickled = pd.read_pickle(dictdir + 'mad.pickle')
    return pickled



def get_encoded_image(fileName):
    filenames =fileName
    # assure_dir(nimgdir)
    # try:
    #     os.remove(dictdir + 'mad.pickle')
    # except FileNotFoundError as e:
    #     pass
    new_dict = phasher.encode_images(nimgdir)
    phashed_learning = get_dict()
    combined_dict = {**phashed_learning, **new_dict}
    print('comb DIct: ',combined_dict)
    return combined_dict, filenames


def dquantum_map(fileName):
    print("FILE NAME :-- ",fileName)
    combined_dict, filenames = get_encoded_image(fileName)
    diff_duplicates = phasher.find_duplicates(encoding_map=combined_dict)
    print("DIFF ",diff_duplicates)
    try:
        match_name = diff_duplicates[fileName][0]
        print("match_name:  ",match_name)
        print("file Names:  ",fileName)
        a = match_name
        if a in filenames:
            match_name = diff_duplicates[fileName][2]      
        else:
            match_name = match_name
    except IndexError as e:
        print("No available template",e)
        diff_duplicates = phasher.find_duplicates(encoding_map=combined_dict ,max_distance_threshold=35, scores=True)
        match_name = diff_duplicates[fileName][0]
    return match_name

# def remove_bkg(source):
#     x=imgdir+'/sss.jpg'
#     src = cv2.imread(source, 1)
#     tmp = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
#     _,alpha = cv2.threshold(tmp,0,255,cv2.THRESH_BINARY)
#     b, g, r = cv2.split(src)
#     rgba = [b,g,r, alpha]
#     dst = cv2.merge(rgba,4)
#     cv2.imwrite(x,dst )
# #     # name=source.split('/');
# #     # img = cv2.imread(source)
# #     # mask = np.zeros(img.shape[:2],np.uint8)
# #     # bgdModel = np.zeros((1,65),np.float64)
# #     # fgdModel = np.zeros((1,65),np.float64)
# #     # rect = (50,50,450,290)
# #     # cv2.grabCut(img,mask,rect,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_RECT)
# #     # mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
# #     # img = img*mask2[:,:,np.newaxis]
# #     # cv2.imwrite(source,img )
# #     # foreground = cv2.imread(source)
# #     # oreground = cv2.cvtColor(foreground, cv2.COLOR_BGR2RGB)
# #     # foreground = cv2.resize(foreground,(r.shape[1],r.shape[0]))
# #     # background = 255 * np.ones_like(rgb).astype(np.uint8)
# #     # foreground = foreground.astype(float)
# #     # background = background.astype(float)
# #     # th, alpha = cv2.threshold(np.array(rgb),0,255, cv2.THRESH_BINARY)
# #     # alpha = cv2.GaussianBlur(alpha, (7,7),0)
# #     # alpha = alpha.astype(float)/255
# #     # foreground = cv2.multiply(alpha, foreground)
# #     # background = cv2.multiply(1.0 - alpha, background)
# #     # outImage = cv2.add(foreground, background)
# #     # with open(pimgdir + outImage, 'wb') as file:
# #     #     z = file.write(outImage/255)
    


# # def transform(source):
# # # This function is used to find the corners of the object
# # # and the dimensions of the object
# #     image = cv2.imread(source)
# #     print(image)
# #     gray=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
# #     edged = cv2.Canny(image, 1000, 1000)
# #     (cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
# #     idx = 0
# #     for c in cnts:
# #         x,y,w,h = cv2.boundingRect(c)
# #         if w>50 and h>50:
# #             idx+=1
# #             new_img=image[y:y+h,x:x+w]
# #             print(new_img)
# #             cv2.imwrite(str(source) + '.png', new_img)
# #     cv2.imwrite((source),image)
# #     cv2.waitKey(0)


# # if __name__ == '__main__':
# #     transform(imgdir+'/office.jpg')








